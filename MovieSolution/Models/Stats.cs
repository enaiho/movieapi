﻿
namespace MovieSolution.Models
{
    public class Stats
    {
        public int MovieId { get; set; }
        public int WatchDurationMs { get; set; }

    }
}
